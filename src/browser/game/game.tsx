import * as React from 'react';
import { connect } from 'react-redux';
import { Field } from '../components';
import { State } from '../../lib';
import { Field as iField } from '../../lib/game/reducer';

interface Props {
  fields: Array<Array<iField>>;
}

const Game: React.SFC<Props> = ({ fields }) => (
  <div style={{ display: 'flex' }}>
    {fields.map((row: Array<iField>, i: number) => (
      <div key={`row_${i}`}>
        {row.map((field: iField, j: number) => (
          <Field {...{ field, key: `field_${j}` }} />
        ))}
      </div>
    ))}
  </div>
);

const mapStateProps = (state: State) => ({
  fields: state.game.fields,
});

export default connect(mapStateProps)(Game);
