import * as React from 'react';
import * as Radium from 'radium';

const scoreStyles: React.CSSProperties = {
  marginLeft: 'auto',
};

const Score: React.SFC = () => <div style={scoreStyles}>Score</div>;

export default Radium(Score);
