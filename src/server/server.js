import webpack from 'webpack';
import express from 'express';
import devMiddleware from 'webpack-dev-middleware';
import hotMiddleware from 'webpack-hot-middleware';
import config from './webpack.config';

const compiler = webpack(config);
const app = express();

app.use(devMiddleware(compiler));
app.use(hotMiddleware(compiler));

export default app;
