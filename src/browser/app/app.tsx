import * as React from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import { Game } from '../game';
import { Menu } from '../menu';
import reducers from '../../lib';

const store = createStore(reducers, applyMiddleware(logger));
store.dispatch({ type: 'START_GAME' });

const App: React.SFC = () => (
  <Provider {...{ store }}>
    <div style={{ textAlign: 'center' }}>
      <div style={{ display: 'inline-block' }}>
        <Menu prop="someProp" />
        <Game />
      </div>
    </div>
  </Provider>
);

export default App;
