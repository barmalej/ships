export interface GameAction {
  type: string;
  payload: any;
}

export interface Field {
  coords: { x: number, y: number };
}

export interface GameState {
  started: boolean;
  fields: Array<Array<Field>>;
}

type GameReducer = (state: GameState, action: GameAction) => GameState;

const genFields = () => {
  const fields = [];
  for (let x = 0; x < 10; x++) {
    const row = [];
    for (let y = 0; y < 10; y++) {
      row.push({ coords: { x, y } });
    }
    fields.push(row);
  }
  return fields;
};

const initialState = {
  started: false,
  fields: genFields(),
};

const gameReducer: GameReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'START_GAME':
      return { ...state, started: true };
    default:
      return state;
  }
};

export default gameReducer;
