import * as React from 'react';
import * as Radium from 'radium';

const buttonStyle: React.CSSProperties = {
  padding: '10px',
  borderRadius: '3px',
  backgroundColor: 'lightgreen',
  color: 'white',
  fontSize: '16px',
};

interface Props {
  tag?: string;
  style?: React.CSSProperties;
}

const Button: React.SFC<Props> = ({
  tag: Tag = 'button',
  children,
  style,
  ...otherProps
}) => (
  <Tag {...{ ...otherProps, style: { ...buttonStyle, ...style } }}>
    {children}
  </Tag>
);

export default Radium(Button);
