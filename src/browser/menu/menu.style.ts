interface Styles {
  wrapper: React.CSSProperties;
  dashboard: React.CSSProperties;
  startButton: React.CSSProperties;
}

const menuStyles: Styles = {
  wrapper: {
    padding: '20px',
    borderWidth: '1px',
    borderStyle: 'solid',
  },
  dashboard: {
    textAlign: 'left',
    display: 'flex',
    // background: '#EEA200',
    // 'margin-top': '40px',
    // padding: '24px',
    // 'text-align': 'center',
    // 'font-family': 'Raleway',
    // 'box-shadow': '2px 2px 8px rgba(0, 0, 0, 0.5)',
  },
  startButton: {
    ':hover': {
      backgroundColor: '#ffffff',
      color: '#EEA200',
      // padding: '24px 10px',
    },
    // transition: '0.4s',
    // color: '#ffffff',
    // 'font-size': '20px',
    // 'text-decoration': 'none',
    // padding: '0 10px',
    // margin: '0 10px',
  },
};

export default menuStyles;
