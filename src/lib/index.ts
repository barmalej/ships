import { combineReducers } from 'redux';
import app from '../lib/app/reducer';
import game from '../lib/game/reducer';
import { AppState } from '../lib/app/reducer';
import { GameState } from '../lib/game/reducer';

export interface State {
  readonly app: AppState,
  readonly game: GameState,
}

const reducers = combineReducers<State>({
  app,
  game,
});

export default reducers;
