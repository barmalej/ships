import * as React from 'react';
import * as Radium from 'radium';
import { Button, Input } from '../components';
import menuStyles from './menu.style';
import Score from './score';

interface Props {
  prop: string;
}

const Menu: React.SFC<Props> = ({ prop }) => (
  <div style={menuStyles.wrapper}>
    <h1>Ships</h1>
    <nav style={menuStyles.dashboard}>
      <Button style={menuStyles.startButton}>Start</Button>
      <Score />
    </nav>
  </div>
);

export default Radium(Menu);
