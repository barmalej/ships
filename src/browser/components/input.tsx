import * as React from 'react';
import * as Radium from 'radium';

interface Styles {
  inputStyles
}

const inputStyles = {
  color: 'green'
};

interface Props {
  prop: string,
}

const Input: React.SFC<Props> = (props) => (
  <div style={inputStyles}>
    <input />
  </div>
);

export default Radium(Input);
