interface AppAction {
  type: string;
}
export interface AppState {
  word: string;
}

type AppReducer = (state: AppState, action: AppAction) => AppState;

const initialState = {
  word: 'BURDA',
};

const appReducer: AppReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default appReducer;
