declare var module: any;
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './app';

const appEl = document.getElementById('app');

ReactDOM.render(<App />, appEl);

if (module.hot) {
  module.hot.accept('./app', () => {
    const NextApp = require('./app').App;
    ReactDOM.render(<NextApp />, appEl);
  });
}
