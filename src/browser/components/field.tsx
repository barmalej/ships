import * as React from 'react';
import * as Radium from 'radium';
import { Field as iField } from '../../lib/game/reducer';

interface Props {
  field: iField;
}

const fieldStyles: React.CSSProperties = {
  backgroundColor: 'lightblue',
  width: '50px',
  height: '50px',
  borderWidth: '1px',
  borderColor: '#eee',
  borderStyle: 'solid',
  ':hover': {
    boxShadow: '0 0 15px 1px inset',
  },
};

const Field: React.SFC<Props> = ({ field }) => <div style={fieldStyles} />;

export default Radium(Field);
